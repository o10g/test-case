# TAO challange

## Installation

1. Clone the repository
1. Run `composer install`
1. To start the application run `php bin/console server:start` from the project root
1. Application is available on `http://localhost:8000`

## Configuration

Data source used without changing configuration is `JSON`.
To change the data source you need to change a service definition in a file `config/services.yml`.

Change the `App\Domain\Repository\TestTakersRepository` implementation to one of existing implementations:

1. `'@App\Application\Repository\JsonTestTakersRepository'`
1. `'@App\Application\Repository\CsvTestTakersRepository'`

You can have your own data source. Just implement `App\Domain\Repository\TestTakersRepository` interface and use 
the implementation in the `services.yaml` for the interface.
 
## Run the application

Application is run by the command  `php bin/console server:start` from the project root

Two different views are shown by URLs:

http://localhost:8000/test_takers.json 
http://localhost:8000/test_takers.xml

## Testing

`php bin/phpunit`

## To Do

* Error/Exceptions handling on UI layer
* DataSource factory based on config value, not service definitions 

## Feedback from the company

In summary, it's done very well.

### Nice
+++ unit tests

+++ good code structure, class hierarchy, factory

++ XML built over YML

\+ Symfony

\+ value objects for all fields

\+ nice README

\+ declare(strict_types=1);

\+ different commits for the app and the framework 

\+ getRequestFormat instead of an extra parameter

### Improve

\- CSVReader is not replaceable in CsvTestTakersRepository

\- XmlResponse::update is private and never called within the class
