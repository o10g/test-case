<?php
declare(strict_types=1);

namespace App\Domain\Model;

use App\Domain\Model\Profile\Address;
use App\Domain\Model\Profile\EmailAddress;
use App\Domain\Model\Profile\FirstName;
use App\Domain\Model\Profile\Gender;
use App\Domain\Model\Profile\LastName;
use App\Domain\Model\Profile\Login;
use App\Domain\Model\Profile\Password;
use App\Domain\Model\Profile\Picture;
use App\Domain\Model\Profile\Title;

class TestTaker implements \JsonSerializable
{
    private $login;
    private $password;
    private $lastName;
    private $firstName;
    private $gender;
    private $email;
    private $picture;
    private $address;
    private $title;

    public function __construct(
        Login $login,
        Password $password,
        Title $title,
        LastName $lastName,
        FirstName $firstName,
        Gender $gender,
        EmailAddress $email,
        Picture $picture,
        Address $address
    ) {
        $this->login = $login;
        $this->password = $password;
        $this->lastName = $lastName;
        $this->firstName = $firstName;
        $this->gender = $gender;
        $this->email = $email;
        $this->picture = $picture;
        $this->address = $address;
        $this->title = $title;
    }

    public function jsonSerialize()
    {
        return [
            'login' => $this->login->value(),
            'password' => $this->password->value(),
            'title' => $this->title->value(),
            'lastname' => $this->lastName->value(),
            'firstname' => $this->firstName->value(),
            'gender' => $this->gender->value(),
            'email' => $this->email->value(),
            'picture' => $this->picture->value(),
            'address' => $this->address->value(),
        ];
    }

    public function login(): Login
    {
        return $this->login;
    }

    public function password(): Password
    {
        return $this->password;
    }

    public function lastName(): LastName
    {
        return $this->lastName;
    }

    public function firstName(): FirstName
    {
        return $this->firstName;
    }

    public function gender(): Gender
    {
        return $this->gender;
    }

    public function email(): EmailAddress
    {
        return $this->email;
    }

    public function picture(): Picture
    {
        return $this->picture;
    }

    public function address(): Address
    {
        return $this->address;
    }

    public function title(): Title
    {
        return $this->title;
    }
}
