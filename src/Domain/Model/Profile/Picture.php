<?php
declare(strict_types=1);


namespace App\Domain\Model\Profile;

use InvalidArgumentException;

class Picture
{
    private $value;

    public function __construct(string $value)
    {
        $this->ensureValidUrl($value);
        $this->value = $value;
    }

    public function value(): string
    {
        return $this->value;
    }

    public function __toString()
    {
        return $this->value;
    }

    private function ensureValidUrl(string $value)
    {
        if (!filter_var($value, FILTER_VALIDATE_URL)) {
            throw new InvalidArgumentException(sprintf('The image url <%s> is not valid', $value));
        }
    }
}
