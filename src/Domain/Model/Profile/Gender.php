<?php
declare(strict_types=1);


namespace App\Domain\Model\Profile;

use InvalidArgumentException;

class Gender
{
    const GENDER_MALE = 'male';
    const GENDER_FEMALE = 'female';

    private $value;

    public function __construct(string $value)
    {
        $this->ensureIsValidGender($value);
        $this->value = $value;
    }

    public function value(): string
    {
        return $this->value;
    }

    public function __toString()
    {
        return $this->value;
    }

    private function ensureIsValidGender(string $value)
    {
        if (!in_array($value, [self::GENDER_FEMALE, self::GENDER_MALE])) {
            throw new InvalidArgumentException(sprintf('The gender <%s> is not valid', $value));
        }
    }
}
