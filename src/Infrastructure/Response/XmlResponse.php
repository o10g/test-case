<?php
declare(strict_types=1);

namespace App\Infrastructure\Response;

use Symfony\Component\HttpFoundation\Response;

class XmlResponse extends Response
{
    public function __construct($content = '', int $status = 200, array $headers = [])
    {
        parent::__construct($content, $status, $headers);
    }

    private function update()
    {
        $this->headers->set('Content-Type', 'application/xml');

        return $this->setContent($this->content);
    }
}
