<?php
declare(strict_types=1);

namespace App\Infrastructure\Factory;

use App\Infrastructure\Response\XmlResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Twig\Environment;

class ResponseFactory
{
    private $twig;

    public function __construct(Environment $twig)
    {
        $this->twig = $twig;
    }

    public function create(string $format, array $data): Response
    {
        switch ($format) {
            case 'json':
                return new JsonResponse($data);

            case 'xml':
                return new XmlResponse($this->twig->render('test_takers/list.xml.twig', [
                    'test_takers' => $data
                ]));

            default:
                throw new UnsupportedFormatException(sprintf('%s format is not supported', $format));
        }
    }
}
