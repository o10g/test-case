<?php
declare(strict_types=1);

namespace App\Infrastructure\Controller;

use App\Domain\Repository\TestTakersRepository;
use App\Infrastructure\Factory\ResponseFactory;
use Symfony\Component\HttpFoundation\Request;

class TestTakerListAllController
{
    private $repository;
    private $responseFactory;

    public function __construct(TestTakersRepository $repository, ResponseFactory $responseFactory)
    {
        $this->repository = $repository;
        $this->responseFactory = $responseFactory;
    }

    public function __invoke(Request $request)
    {
        $requestedFormat = $request->getRequestFormat('json');
        return $this->responseFactory->create($requestedFormat, $this->repository->findAll());
    }
}
