<?php
declare(strict_types=1);


namespace App\Application\Repository;

class FileNotFoundException extends \Exception
{
}
