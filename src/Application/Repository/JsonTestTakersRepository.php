<?php
declare(strict_types=1);

namespace App\Application\Repository;

use App\Application\Parser\TestTakerParser;
use App\Domain\Repository\TestTakersRepository;

class JsonTestTakersRepository implements TestTakersRepository
{
    private $filename;
    private $parser;

    public function __construct(string $filename, TestTakerParser $parser)
    {
        if (!file_exists($filename)) {
            throw new FileNotFoundException(sprintf('File %s is not found', $filename));
        }
        $this->filename = $filename;
        $this->parser = $parser;
    }

    public function findAll(): array
    {
        $data = $this->readFile();

        $testTakers = [];
        foreach ($data as $testTaker) {
            $testTakers[] = $this->parser->parse($testTaker);
        }

        return $testTakers;
    }

    private function readFile(): array
    {
        $data = json_decode(file_get_contents($this->filename), true);
        $jsonError = json_last_error();
        if ($jsonError !== JSON_ERROR_NONE) {
            throw new FileNotValidException(sprintf('Json parsing error code: %s', $jsonError));
        }

        return $data;
    }
}
