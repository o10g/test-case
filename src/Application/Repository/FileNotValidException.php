<?php
declare(strict_types=1);

namespace App\Application\Repository;

use Exception;

class FileNotValidException extends Exception
{
}
