<?php
declare(strict_types=1);


namespace App\Application\Repository;

use App\Application\Parser\TestTakerParser;
use App\Domain\Repository\TestTakersRepository;
use League\Csv\AbstractCsv;
use League\Csv\Reader;

class CsvTestTakersRepository implements TestTakersRepository
{
    private $filename;
    private $parser;

    public function __construct(string $filename, TestTakerParser $parser)
    {
        if (!file_exists($filename)) {
            throw new FileNotFoundException(sprintf('File %s not found', $filename));
        }

        $this->filename = $filename;
        $this->parser = $parser;
    }

    public function findAll(): array
    {
        $csv = Reader::createFromPath($this->filename);
        if ($csv->count() === 0) {
            return [];
        }
        $csv->skipInputBOM();
        $csv->setHeaderOffset(0);
        $testTakers = [];

        foreach ($csv as $testTaker) {
            $testTakers[] = $this->parser->parse($testTaker);
        }

        return $testTakers;
    }
}
