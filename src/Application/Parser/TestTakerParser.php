<?php
declare(strict_types=1);


namespace App\Application\Parser;

use App\Domain\Model\Profile\Address;
use App\Domain\Model\Profile\EmailAddress;
use App\Domain\Model\Profile\FirstName;
use App\Domain\Model\Profile\Gender;
use App\Domain\Model\Profile\LastName;
use App\Domain\Model\Profile\Login;
use App\Domain\Model\Profile\Password;
use App\Domain\Model\Profile\Picture;
use App\Domain\Model\TestTaker;
use App\Domain\Model\Profile\Title;
use InvalidArgumentException;

class TestTakerParser
{
    public function parse(array $data): TestTaker
    {
        $this->ensureValid($data);
        $data = $this->filterKeys($data);
        return new TestTaker(
            new Login($data['login']),
            new Password($data['password']),
            new Title($data['title']),
            new LastName($data['lastname']),
            new FirstName($data['firstname']),
            new Gender($data['gender']),
            new EmailAddress($data['email']),
            new Picture($data['picture']),
            new Address($data['address'])
        );
    }

    private function ensureValid(array $data)
    {
        if ($result = array_diff($this->requiredIndexes(), $this->getDataArrayKeys($data))) {
            throw new InvalidArgumentException(
                sprintf('Required keys %s are missed', implode(', ', $result))
            );
        }
    }

    private function requiredIndexes(): array
    {
        return [
            'login',
            'password',
            'title',
            'lastname',
            'firstname',
            'gender',
            'email',
            'picture',
            'address',
        ];
    }

    private function getDataArrayKeys(array $data): array
    {
        return array_map('trim', array_keys($data));
    }

    private function filterKeys(array $data)
    {
        return array_combine(array_map('trim', array_keys($data)), $data);
    }
}
