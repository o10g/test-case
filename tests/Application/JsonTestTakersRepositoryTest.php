<?php
declare(strict_types=1);

namespace App\Tests\Application;

use App\Application\Parser\TestTakerParser;
use App\Application\Repository\FileNotFoundException;
use App\Application\Repository\FileNotValidException;
use App\Application\Repository\JsonTestTakersRepository;
use App\Domain\Model\TestTaker;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use PHPUnit\Util\Json;

class JsonTestTakersRepositoryTest extends TestCase
{
    public function test_i_can_not_create_repository_without_file()
    {
        $this->expectException(FileNotFoundException::class);
        new JsonTestTakersRepository(
            'not_existing_file',
            $this->getParserMock()
        );
    }

    public function test_empty_file()
    {
        $repo = new JsonTestTakersRepository(
            __DIR__ . '/resources/empty.json',
            $this->getParserMock()
        );

        $this->assertCount(0, $repo->findAll());
    }

    public function test_file_with_one_test_taker()
    {
        $repo = new JsonTestTakersRepository(
            __DIR__ . '/resources/one_test_taker.json',
            $this->getParserMock()
        );

        $this->assertCount(1, $repo->findAll());
    }

    public function test_repository_returns_only_test_takers()
    {
        $repo = new JsonTestTakersRepository(
            __DIR__ . '/resources/one_test_taker.json',
            $this->getParserMock()
        );

        $this->assertContainsOnlyInstancesOf(TestTaker::class, $repo->findAll());
    }

    public function test_invalid_json_should_warn()
    {
        $this->expectException(FileNotValidException::class);
        $repo = new JsonTestTakersRepository(
            __DIR__ . '/resources/invalid.json',
            $this->getParserMock()
        );

        $repo->findAll();
    }

    /**
     * @return MockObject|TestTakerParser
     */
    private function getParserMock(): MockObject
    {
        return $this->createMock(TestTakerParser::class);
    }
}
