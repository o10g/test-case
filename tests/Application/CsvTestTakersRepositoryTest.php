<?php

namespace App\Tests\Application;

use App\Application\Parser\TestTakerParser;
use App\Application\Repository\CsvTestTakersRepository;
use App\Domain\Model\TestTaker;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class CsvTestTakersRepositoryTest extends TestCase
{
    /** @expectedException App\Application\Repository\FileNotFoundException */
    public function test_i_can_not_create_repository_without_file()
    {
        new CsvTestTakersRepository(
            'not_existing_file',
            $this->getParserMock()
        );
    }

    public function test_empty_file()
    {
        $repo = new CsvTestTakersRepository(
            __DIR__ . '/resources/empty.csv',
            $this->getParserMock()
        );

        $this->assertCount(0, $repo->findAll());
    }

    public function test_file_with_one_test_taker()
    {
        $repo = new CsvTestTakersRepository(
            __DIR__ . '/resources/one_test_taker.csv',
            $this->getParserMock()
        );

        $this->assertCount(1, $repo->findAll());
    }

    public function test_repository_returns_only_test_takers()
    {
        $repo = new CsvTestTakersRepository(
            __DIR__ . '/resources/one_test_taker.csv',
            $this->getParserMock()
        );

        $this->assertContainsOnlyInstancesOf(TestTaker::class, $repo->findAll());
    }

    /**
     * @return MockObject|TestTakerParser
     */
    private function getParserMock(): MockObject
    {
        return $this->createMock(TestTakerParser::class);
    }
}
