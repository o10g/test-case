<?php

namespace App\Tests\Application;

use App\Application\Parser\TestTakerParser;
use App\Domain\Model\Profile\EmailAddress;
use App\Domain\Model\Profile\FirstName;
use App\Domain\Model\TestTaker;
use App\Tests\Infra\Shared\DataMother\AddressMother;
use App\Tests\Infra\Shared\DataMother\EmailAddressMother;
use App\Tests\Infra\Shared\DataMother\FirstNameMother;
use App\Tests\Infra\Shared\DataMother\GenderMother;
use App\Tests\Infra\Shared\DataMother\LastNameMother;
use App\Tests\Infra\Shared\DataMother\LoginMother;
use App\Tests\Infra\Shared\DataMother\PasswordMother;
use App\Tests\Infra\Shared\DataMother\PictureMother;
use App\Tests\Infra\Shared\DataMother\TitleMother;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

class TestTakerParserTest extends TestCase
{
    public function test_csv_array_to_test_taker()
    {
        $data = $this->getTestTakerRawData();
        $parser = new TestTakerParser();

        $this->assertInstanceOf(TestTaker::class, $parser->parse($data));
    }

    /** @expectedException InvalidArgumentException */
    public function test_invalid_array_should_throw_exception()
    {
        $parser = new TestTakerParser();

        $parser->parse(['invalid' => 'invalid']);
    }

    public function test_address_key_has_caret_return_character()
    {
        $parser = new TestTakerParser();

        $changedData = $this->getTestTakerRawData();
        $changedData['address' . chr(13)] = $changedData['address'];
        unset($changedData['address']);

        $this->assertInstanceOf(TestTaker::class, $parser->parse($changedData));
    }

    private function getTestTakerRawData(): array
    {
        $data = [
            'login' => LoginMother::random()->value(),
            'password' => PasswordMother::random()->value(),
            'title' => TitleMother::random()->value(),
            'lastname' => LastNameMother::random()->value(),
            'firstname' => FirstNameMother::random()->value(),
            'gender' => GenderMother::random()->value(),
            'email' => EmailAddressMother::random()->value(),
            'picture' => PictureMother::random()->value(),
            'address' => AddressMother::random()->value(),
        ];

        return $data;
    }
}
