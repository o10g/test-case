<?php
declare(strict_types=1);

namespace App\Tests\Infra\Shared\DataMother;

use App\Domain\Model\Profile\Gender;

class GenderMother
{
    public static function create(string $gender): Gender
    {
        return new Gender($gender);
    }

    public static function random(): Gender
    {
        return self::create(MotherCreator::random()->randomElement(
            ['female', 'male']
        ));
    }
}
