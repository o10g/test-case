<?php
declare(strict_types=1);

namespace App\Tests\Infra\Shared\DataMother;

class WordMother
{
    public static function random(): string
    {
        return MotherCreator::random()->word;
    }
}
