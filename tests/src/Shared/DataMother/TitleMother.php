<?php
declare(strict_types=1);

namespace App\Tests\Infra\Shared\DataMother;

use App\Domain\Model\Profile\Title;

class TitleMother
{
    public static function create(string $title): Title
    {
        return new Title($title);
    }

    public static function random(): Title
    {
        return self::create(MotherCreator::random()->title);
    }
}
