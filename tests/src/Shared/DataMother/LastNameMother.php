<?php
declare(strict_types=1);

namespace App\Tests\Infra\Shared\DataMother;

use App\Domain\Model\Profile\LastName;

class LastNameMother
{
    public static function create(string $lastName): LastName
    {
        return new LastName($lastName);
    }

    public static function random(): LastName
    {
        return self::create(MotherCreator::random()->lastName);
    }
}
