<?php
declare(strict_types=1);

namespace App\Tests\Infra\Shared\DataMother;

use App\Domain\Model\Profile\Picture;

class PictureMother
{
    public static function create(string $pictureUrl): Picture
    {
        return new Picture($pictureUrl);
    }

    public static function random(): Picture
    {
        return self::create(MotherCreator::random()->imageUrl());
    }
}
