<?php
declare(strict_types=1);

namespace App\Tests\Infra\Shared\DataMother;

use App\Domain\Model\Profile\EmailAddress;

class EmailAddressMother
{
    public static function create(string $email): EmailAddress
    {
        return new EmailAddress($email);
    }

    public static function random(): EmailAddress
    {
        return self::create(MotherCreator::random()->email);
    }
}
