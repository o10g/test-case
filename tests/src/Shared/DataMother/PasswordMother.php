<?php
declare(strict_types=1);

namespace App\Tests\Infra\Shared\DataMother;

use App\Domain\Model\Profile\Password;

class PasswordMother
{
    public static function create(string $password): Password
    {
        return new Password($password);
    }

    public static function random(): Password
    {
        return self::create(MotherCreator::random()->password);
    }
}
