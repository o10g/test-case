<?php
declare(strict_types=1);

namespace App\Tests\Infra\Shared\DataMother;

use App\Domain\Model\Profile\Address;

class AddressMother
{
    public static function create(string $address): Address
    {
        return new Address($address);
    }

    public static function random(): Address
    {
        return self::create(MotherCreator::random()->address);
    }
}
