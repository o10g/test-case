<?php
declare(strict_types=1);

namespace App\Tests\Infra\Shared\DataMother;

use App\Domain\Model\Profile\Login;

class LoginMother
{
    public static function create(string $login): Login
    {
        return new Login($login);
    }
    public static function random(): Login
    {
        return self::create(WordMother::random());
    }
}
