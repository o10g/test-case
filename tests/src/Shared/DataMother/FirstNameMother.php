<?php
declare(strict_types=1);

namespace App\Tests\Infra\Shared\DataMother;

use App\Domain\Model\Profile\FirstName;

class FirstNameMother
{
    public static function create(string $firstName): FirstName
    {
        return new FirstName($firstName);
    }

    public static function random(): FirstName
    {
        return self::create(MotherCreator::random()->firstName);
    }
}
