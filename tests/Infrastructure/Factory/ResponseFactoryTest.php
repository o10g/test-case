<?php
declare(strict_types=1);

namespace App\Tests\Infrastructure\Factory;

use App\Infrastructure\Factory\ResponseFactory;
use App\Infrastructure\Factory\UnsupportedFormatException;
use App\Infrastructure\Response\XmlResponse;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Twig\Environment;

class ResponseFactoryTest extends TestCase
{
    /**
     * @var ResponseFactory
     */
    private $factory;
    /**
     * @var MockObject|Environment
     */
    private $rendererMock;

    public function setUp()
    {
        $this->rendererMock = $this->createMock(Environment::class);
        $this->factory = new ResponseFactory($this->rendererMock);
    }

    public function test_i_can_create_json_response()
    {
        $this->assertInstanceOf(
            JsonResponse::class,
            $this->factory->create('json', [])
        );
    }

    public function test_i_can_create_xml_response()
    {
        $this->assertInstanceOf(
            XmlResponse::class,
            $this->factory->create('xml', [])
        );
    }

    public function test_unsupported_format_throw_exception()
    {
        $this->expectException(UnsupportedFormatException::class);

        $this->factory->create('html', []);
    }

    public function test_json_response_has_data_after_creation()
    {
        $response = $this->factory->create('json', ['not empty array']);

        $this->assertFalse(in_array($response->getContent(), ['""', '""', '{}', '[]']));
    }

    public function test_xml_response_with_empty_data()
    {
        $response = $this->factory->create('xml', []);

        $this->assertTrue(in_array($response->getContent(), ['']));
    }

    public function test_xml_response_has_data_after_creation()
    {
        $this->rendererMock->method('render')->willReturnCallback(function ($template, $context) {
            return json_encode($context['test_takers']);
        });

        $response = $this->factory->create('xml', ['not empty array']);

        $this->assertFalse(in_array($response->getContent(), ['', '""']));
    }
}
