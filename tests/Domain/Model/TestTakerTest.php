<?php

namespace App\Tests\Domain\Model;

use App\Domain\Model\TestTaker;
use App\Tests\Infra\Shared\DataMother\AddressMother;
use App\Tests\Infra\Shared\DataMother\EmailAddressMother;
use App\Tests\Infra\Shared\DataMother\FirstNameMother;
use App\Tests\Infra\Shared\DataMother\GenderMother;
use App\Tests\Infra\Shared\DataMother\LastNameMother;
use App\Tests\Infra\Shared\DataMother\LoginMother;
use App\Tests\Infra\Shared\DataMother\PasswordMother;
use App\Tests\Infra\Shared\DataMother\PictureMother;
use App\Tests\Infra\Shared\DataMother\TitleMother;
use PHPUnit\Framework\TestCase;

class TestTakerTest extends TestCase
{
    public function test_i_can_create_test_taker()
    {
        $testTaker = new TestTaker(
            LoginMother::random(),
            PasswordMother::random(),
            TitleMother::random(),
            LastNameMother::random(),
            FirstNameMother::random(),
            GenderMother::random(),
            EmailAddressMother::random(),
            PictureMother::random(),
            AddressMother::random()
        );
        $this->assertInstanceOf(TestTaker::class, $testTaker);
    }

    public function test_i_can_decode_to_json()
    {
        $reference = $this->referenceData();
        $testTaker = new TestTaker(
            LoginMother::create($reference['login']),
            PasswordMother::create($reference['password']),
            TitleMother::create($reference['title']),
            LastNameMother::create($reference['lastname']),
            FirstNameMother::create($reference['firstname']),
            GenderMother::create($reference['gender']),
            EmailAddressMother::create($reference['email']),
            PictureMother::create($reference['picture']),
            AddressMother::create($reference['address'])
        );

        $this->assertEquals($this->referenceData(), json_decode(json_encode($testTaker), true));
    }

    public function referenceData()
    {
        return [
            "login" => "grahamallison",
            "password" => "123456",
            "title" => "ms",
            "lastname" => "graham",
            "firstname" => "allison",
            "gender" => "female",
            "email" => "allison.graham70@example.com",
            "picture" => "http://api.randomuser.me/0.2/portraits/women/35.jpg",
            "address" => "6697 rolling green rd colorado springs 56306",
        ];
    }
}
